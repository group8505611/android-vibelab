import kotlin.concurrent.thread
import java.util.concurrent.TimeUnit

fun main() {
    // Запускаем фоновый поток
    thread(start = true) {
        TimeUnit.SECONDS.sleep(1)
        println("World")
    }

    // Печатаем "Hello," с задержкой в 2 секунды и блокируем поток
    TimeUnit.SECONDS.sleep(2)
    print("Hello, ")
}