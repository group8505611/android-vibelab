import kotlin.system.measureTimeMillis
import kotlinx.coroutines.*

// Функция, возвращающая первое число с задержкой
suspend fun getNumberOne(): Int {
    print("I'm sleeping 1 ... ")
    delay(2000) // Задержка на 2 секунды
    return 10
}

// Функция, возвращающая второе число с задержкой
suspend fun getNumberTwo(): Int {
    print("I'm sleeping 2 ... ")
    delay(3000) // Задержка на 3 секунды
    return 20
}

fun main() {
    // Последовательный вызов функций
    val sequentialTime = measureTimeMillis {
        val numberOne = runBlocking { getNumberOne() }
        val numberTwo = runBlocking { getNumberTwo() }
        println("Последовательный вызов: ${numberOne + numberTwo}")
    }
    println("Время выполнения последовательного вызова: $sequentialTime мс")
    println("main: I'm tired of waiting!")

    // Асинхронный вызов функций
    val asyncTime = measureTimeMillis {
        val numberOneDeferred = GlobalScope.async { getNumberOne() }
        val numberTwoDeferred = GlobalScope.async { getNumberTwo() }
        runBlocking {
            val sum = numberOneDeferred.await() + numberTwoDeferred.await()
            println("Асинхронный вызов: $sum")
        }
    }
    println("Время выполнения асинхронного вызова: $asyncTime мс")
    println("main: Now I can quit.")
}
